(function (doc) {
    doc.addEventListener('DOMContentLoaded', function() {

        const dataset = 'dataset',
            clas = 'classList',
            activeClass = 'active',
            parentNode = 'parentNode',
            text = 'textContent',
            style = 'style',
            checked = 'checked',
            addClass = 'add',
            removeClass = 'remove';

        function match(element, className) {
            return element[clas].contains(className);
        }

        const forms = doc.querySelectorAll('.form');


        for(let form of forms) {
            form.addEventListener('submit', (e) => {
                e.preventDefault()
                const btn = e.target.querySelector('.ct-btn');
                btn[clas][addClass]('load')
                setTimeout(() => {
                    btn[clas][removeClass]('load')
                    btn[clas][addClass]('done')
                    doc.body[clas][addClass]('modal--visible')
                    setTimeout(() => {
                        btn[clas][removeClass]('done')
                    }, 2000)
                }, 2000)

            })
        }
        doc.querySelector('.close-mod').addEventListener('click', function() {
            doc.body[clas][removeClass]('modal--visible')
            this[clas][removeClass]('close-mod')
        })

        // doc.addEventListener('click', (e) => {
        //     const target = e.target;
        //
        //
        // })
    });
})(document)